<?php
/**
 * @file
 * ethical_videos.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function ethical_videos_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: file_display
  $overrides["file_display.video__default__file_field_file_video.status"] = FALSE;
  $overrides["file_display.video__default__file_field_file_video.weight"] = -41;
  $overrides["file_display.video__default__media_vimeo_video.settings|protocol"] = 'http://';
  $overrides["file_display.video__default__media_vimeo_video.weight"] = -43;
  $overrides["file_display.video__default__media_youtube_image.settings|image_style"] = 'medium';
  $overrides["file_display.video__default__media_youtube_image.weight"] = -44;
  $overrides["file_display.video__default__media_youtube_video.settings|captions"] = FALSE;
  $overrides["file_display.video__default__media_youtube_video.settings|modestbranding"] = 1;
  $overrides["file_display.video__default__media_youtube_video.weight"] = -50;
  $overrides["file_display.video__preview__file_field_file_default.status"] = FALSE;
  $overrides["file_display.video__preview__file_field_media_large_icon.status"] = FALSE;
  $overrides["file_display.video__preview__media_vimeo_image.settings|image_style"] = 'panopoly_image_thumbnail';
  $overrides["file_display.video__preview__media_youtube_image.settings|image_style"] = '';
  $overrides["file_display.video__teaser__media_vimeo_video.settings|height"] = 340;
  $overrides["file_display.video__teaser__media_vimeo_video.settings|protocol"] = 'http://';
  $overrides["file_display.video__teaser__media_vimeo_video.settings|protocol_specify"]["DELETED"] = TRUE;
  $overrides["file_display.video__teaser__media_vimeo_video.settings|width"] = 560;
  $overrides["file_display.video__teaser__media_youtube_video.settings|captions"] = FALSE;

  // Exported overrides for: file_type
  $overrides["file_type.video.disabled"] = FALSE;

 return $overrides;
}
