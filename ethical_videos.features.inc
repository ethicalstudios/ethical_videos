<?php
/**
 * @file
 * ethical_videos.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ethical_videos_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "file_entity" && $api == "file_default_displays") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function ethical_videos_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_file_default_displays_alter().
 */
function ethical_videos_file_default_displays_alter(&$data) {
  if (isset($data['video__default__file_field_file_video'])) {
    $data['video__default__file_field_file_video']->status = FALSE; /* WAS: TRUE */
    $data['video__default__file_field_file_video']->weight = -41; /* WAS: 50 */
  }
  if (isset($data['video__default__media_vimeo_video'])) {
    $data['video__default__media_vimeo_video']->settings['protocol'] = 'http://'; /* WAS: 'https://' */
    $data['video__default__media_vimeo_video']->weight = -43; /* WAS: 0 */
  }
  if (isset($data['video__default__media_youtube_image'])) {
    $data['video__default__media_youtube_image']->settings['image_style'] = 'medium'; /* WAS: 'panopoly_image_video' */
    $data['video__default__media_youtube_image']->weight = -44; /* WAS: 1 */
  }
  if (isset($data['video__default__media_youtube_video'])) {
    $data['video__default__media_youtube_video']->settings['captions'] = FALSE; /* WAS: '' */
    $data['video__default__media_youtube_video']->settings['modestbranding'] = 1; /* WAS: 0 */
    $data['video__default__media_youtube_video']->weight = -50; /* WAS: 0 */
  }
  if (isset($data['video__preview__file_field_file_default'])) {
    $data['video__preview__file_field_file_default']->status = FALSE; /* WAS: TRUE */
  }
  if (isset($data['video__preview__file_field_media_large_icon'])) {
    $data['video__preview__file_field_media_large_icon']->status = FALSE; /* WAS: TRUE */
  }
  if (isset($data['video__preview__media_vimeo_image'])) {
    $data['video__preview__media_vimeo_image']->settings['image_style'] = 'panopoly_image_thumbnail'; /* WAS: 'media_thumbnail' */
  }
  if (isset($data['video__preview__media_youtube_image'])) {
    $data['video__preview__media_youtube_image']->settings['image_style'] = ''; /* WAS: 'media_thumbnail' */
  }
  if (isset($data['video__teaser__media_vimeo_video'])) {
    $data['video__teaser__media_vimeo_video']->settings['height'] = 340; /* WAS: 180 */
    $data['video__teaser__media_vimeo_video']->settings['protocol'] = 'http://'; /* WAS: 'https://' */
    $data['video__teaser__media_vimeo_video']->settings['width'] = 560; /* WAS: 320 */
    unset($data['video__teaser__media_vimeo_video']->settings['protocol_specify']);
  }
  if (isset($data['video__teaser__media_youtube_video'])) {
    $data['video__teaser__media_youtube_video']->settings['captions'] = FALSE; /* WAS: '' */
  }
}

/**
 * Implements hook_file_default_types_alter().
 */
function ethical_videos_file_default_types_alter(&$data) {
  if (isset($data['video'])) {
    $data['video']->disabled = FALSE; /* WAS: '' */
  }
}
