<?php
/**
 * @file
 * ethical_videos.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function ethical_videos_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'openethical_video_gallery';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'file_managed';
  $view->human_name = 'Open Ethical video gallery';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['css_class'] = 'es-media-library';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['distinct'] = TRUE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '12';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['style_options']['class'] = 'items';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: Field: Country (field_country) */
  $handler->display->display_options['relationships']['field_country_iso2']['id'] = 'field_country_iso2';
  $handler->display->display_options['relationships']['field_country_iso2']['table'] = 'field_data_field_country';
  $handler->display->display_options['relationships']['field_country_iso2']['field'] = 'field_country_iso2';
  /* Field: File: Name */
  $handler->display->display_options['fields']['filename']['id'] = 'filename';
  $handler->display->display_options['fields']['filename']['table'] = 'file_managed';
  $handler->display->display_options['fields']['filename']['field'] = 'filename';
  $handler->display->display_options['fields']['filename']['label'] = '';
  $handler->display->display_options['fields']['filename']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['filename']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['filename']['element_type'] = '0';
  $handler->display->display_options['fields']['filename']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['filename']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['filename']['link_to_file'] = FALSE;
  /* Field: File: Path */
  $handler->display->display_options['fields']['uri']['id'] = 'uri';
  $handler->display->display_options['fields']['uri']['table'] = 'file_managed';
  $handler->display->display_options['fields']['uri']['field'] = 'uri';
  $handler->display->display_options['fields']['uri']['label'] = '';
  $handler->display->display_options['fields']['uri']['element_type'] = '0';
  $handler->display->display_options['fields']['uri']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['uri']['element_default_classes'] = FALSE;
  /* Field: File: Link */
  $handler->display->display_options['fields']['link']['id'] = 'link';
  $handler->display->display_options['fields']['link']['table'] = 'file_managed';
  $handler->display->display_options['fields']['link']['field'] = 'link';
  $handler->display->display_options['fields']['link']['label'] = '';
  $handler->display->display_options['fields']['link']['element_type'] = '0';
  $handler->display->display_options['fields']['link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['link']['element_default_classes'] = FALSE;
  /* Field: File: File ID */
  $handler->display->display_options['fields']['fid']['id'] = 'fid';
  $handler->display->display_options['fields']['fid']['table'] = 'file_managed';
  $handler->display->display_options['fields']['fid']['field'] = 'fid';
  $handler->display->display_options['fields']['fid']['label'] = '';
  $handler->display->display_options['fields']['fid']['element_type'] = '0';
  $handler->display->display_options['fields']['fid']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['fid']['element_default_classes'] = FALSE;
  $handler->display->display_options['filter_groups']['groups'] = array(
    1 => 'AND',
    2 => 'OR',
  );
  /* Filter criterion: File: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'file_managed';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'video' => 'video',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Field: Project (field_project) */
  $handler->display->display_options['filters']['field_project_target_id']['id'] = 'field_project_target_id';
  $handler->display->display_options['filters']['field_project_target_id']['table'] = 'field_data_field_project';
  $handler->display->display_options['filters']['field_project_target_id']['field'] = 'field_project_target_id';
  $handler->display->display_options['filters']['field_project_target_id']['group'] = 1;
  $handler->display->display_options['filters']['field_project_target_id']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_project_target_id']['expose']['operator_id'] = 'field_project_target_id_op';
  $handler->display->display_options['filters']['field_project_target_id']['expose']['label'] = 'Project';
  $handler->display->display_options['filters']['field_project_target_id']['expose']['operator'] = 'field_project_target_id_op';
  $handler->display->display_options['filters']['field_project_target_id']['expose']['identifier'] = 'field_project_target_id';
  $handler->display->display_options['filters']['field_project_target_id']['expose']['multiple'] = TRUE;
  $handler->display->display_options['filters']['field_project_target_id']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
  );
  /* Filter criterion: Field: Category (field_featured_categories) */
  $handler->display->display_options['filters']['field_featured_categories_tid']['id'] = 'field_featured_categories_tid';
  $handler->display->display_options['filters']['field_featured_categories_tid']['table'] = 'field_data_field_featured_categories';
  $handler->display->display_options['filters']['field_featured_categories_tid']['field'] = 'field_featured_categories_tid';
  $handler->display->display_options['filters']['field_featured_categories_tid']['operator'] = 'and';
  $handler->display->display_options['filters']['field_featured_categories_tid']['group'] = 1;
  $handler->display->display_options['filters']['field_featured_categories_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_featured_categories_tid']['expose']['operator_id'] = 'field_featured_categories_tid_op';
  $handler->display->display_options['filters']['field_featured_categories_tid']['expose']['label'] = 'Category';
  $handler->display->display_options['filters']['field_featured_categories_tid']['expose']['operator'] = 'field_featured_categories_tid_op';
  $handler->display->display_options['filters']['field_featured_categories_tid']['expose']['identifier'] = 'field_featured_categories_tid';
  $handler->display->display_options['filters']['field_featured_categories_tid']['expose']['multiple'] = TRUE;
  $handler->display->display_options['filters']['field_featured_categories_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
  );
  $handler->display->display_options['filters']['field_featured_categories_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_featured_categories_tid']['vocabulary'] = 'panopoly_categories';
  /* Filter criterion: Countries: Name - list */
  $handler->display->display_options['filters']['name_list']['id'] = 'name_list';
  $handler->display->display_options['filters']['name_list']['table'] = 'countries_country';
  $handler->display->display_options['filters']['name_list']['field'] = 'name_list';
  $handler->display->display_options['filters']['name_list']['relationship'] = 'field_country_iso2';
  $handler->display->display_options['filters']['name_list']['group'] = 1;
  $handler->display->display_options['filters']['name_list']['exposed'] = TRUE;
  $handler->display->display_options['filters']['name_list']['expose']['operator_id'] = 'name_list_op';
  $handler->display->display_options['filters']['name_list']['expose']['label'] = 'Country';
  $handler->display->display_options['filters']['name_list']['expose']['operator'] = 'name_list_op';
  $handler->display->display_options['filters']['name_list']['expose']['identifier'] = 'name_list';
  $handler->display->display_options['filters']['name_list']['expose']['multiple'] = TRUE;
  $handler->display->display_options['filters']['name_list']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
  );
  $handler->display->display_options['filters']['name_list']['configuration'] = '1';
  $handler->display->display_options['filters']['name_list']['filter'] = array(
    'enabled' => '1',
    'continents' => array(),
  );
  /* Filter criterion: Field: Group (field_group) */
  $handler->display->display_options['filters']['field_group_tid']['id'] = 'field_group_tid';
  $handler->display->display_options['filters']['field_group_tid']['table'] = 'field_data_field_group';
  $handler->display->display_options['filters']['field_group_tid']['field'] = 'field_group_tid';
  $handler->display->display_options['filters']['field_group_tid']['operator'] = 'and';
  $handler->display->display_options['filters']['field_group_tid']['group'] = 1;
  $handler->display->display_options['filters']['field_group_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_group_tid']['expose']['operator_id'] = 'field_group_tid_op';
  $handler->display->display_options['filters']['field_group_tid']['expose']['label'] = 'Group';
  $handler->display->display_options['filters']['field_group_tid']['expose']['operator'] = 'field_group_tid_op';
  $handler->display->display_options['filters']['field_group_tid']['expose']['identifier'] = 'field_group_tid';
  $handler->display->display_options['filters']['field_group_tid']['expose']['multiple'] = TRUE;
  $handler->display->display_options['filters']['field_group_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
  );
  $handler->display->display_options['filters']['field_group_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_group_tid']['vocabulary'] = 'tag';
  /* Filter criterion: Field: Project (field_project) */
  $handler->display->display_options['filters']['field_project_target_id_1']['id'] = 'field_project_target_id_1';
  $handler->display->display_options['filters']['field_project_target_id_1']['table'] = 'field_data_field_project';
  $handler->display->display_options['filters']['field_project_target_id_1']['field'] = 'field_project_target_id';
  $handler->display->display_options['filters']['field_project_target_id_1']['operator'] = 'not empty';
  $handler->display->display_options['filters']['field_project_target_id_1']['group'] = 2;
  /* Filter criterion: Category not empty */
  $handler->display->display_options['filters']['field_featured_categories_tid_1']['id'] = 'field_featured_categories_tid_1';
  $handler->display->display_options['filters']['field_featured_categories_tid_1']['table'] = 'field_data_field_featured_categories';
  $handler->display->display_options['filters']['field_featured_categories_tid_1']['field'] = 'field_featured_categories_tid';
  $handler->display->display_options['filters']['field_featured_categories_tid_1']['ui_name'] = 'Category not empty';
  $handler->display->display_options['filters']['field_featured_categories_tid_1']['operator'] = 'not empty';
  $handler->display->display_options['filters']['field_featured_categories_tid_1']['value'] = '';
  $handler->display->display_options['filters']['field_featured_categories_tid_1']['group'] = 2;
  $handler->display->display_options['filters']['field_featured_categories_tid_1']['vocabulary'] = 'panopoly_categories';
  /* Filter criterion: Country not empty */
  $handler->display->display_options['filters']['name']['id'] = 'name';
  $handler->display->display_options['filters']['name']['table'] = 'countries_country';
  $handler->display->display_options['filters']['name']['field'] = 'name';
  $handler->display->display_options['filters']['name']['relationship'] = 'field_country_iso2';
  $handler->display->display_options['filters']['name']['ui_name'] = 'Country not empty';
  $handler->display->display_options['filters']['name']['operator'] = 'not empty';
  $handler->display->display_options['filters']['name']['group'] = 2;
  /* Filter criterion: Group not empty */
  $handler->display->display_options['filters']['field_group_tid_1']['id'] = 'field_group_tid_1';
  $handler->display->display_options['filters']['field_group_tid_1']['table'] = 'field_data_field_group';
  $handler->display->display_options['filters']['field_group_tid_1']['field'] = 'field_group_tid';
  $handler->display->display_options['filters']['field_group_tid_1']['ui_name'] = 'Group not empty';
  $handler->display->display_options['filters']['field_group_tid_1']['operator'] = 'not empty';
  $handler->display->display_options['filters']['field_group_tid_1']['value'] = '';
  $handler->display->display_options['filters']['field_group_tid_1']['group'] = 2;
  $handler->display->display_options['filters']['field_group_tid_1']['vocabulary'] = 'tag';

  /* Display: Content pane */
  $handler = $view->new_display('panel_pane', 'Content pane', 'panel_pane_1');
  $handler->display->display_options['pane_title'] = 'List of videos';
  $handler->display->display_options['pane_category']['name'] = 'General';
  $handler->display->display_options['pane_category']['weight'] = '0';
  $handler->display->display_options['allow']['use_pager'] = 0;
  $handler->display->display_options['allow']['items_per_page'] = 'items_per_page';
  $handler->display->display_options['allow']['offset'] = 0;
  $handler->display->display_options['allow']['link_to_view'] = 0;
  $handler->display->display_options['allow']['more_link'] = 0;
  $handler->display->display_options['allow']['path_override'] = 0;
  $handler->display->display_options['allow']['title_override'] = 'title_override';
  $handler->display->display_options['allow']['exposed_form'] = 'exposed_form';
  $handler->display->display_options['allow']['fields_override'] = 0;
  $export['openethical_video_gallery'] = $view;

  return $export;
}
